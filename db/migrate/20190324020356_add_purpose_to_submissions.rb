class AddPurposeToSubmissions < ActiveRecord::Migration[5.2]
  def change
    add_column :submissions, :purpose, :text
  end
end
