class CreateProjectAttachments < ActiveRecord::Migration[5.2]
  def change
    create_table :project_attachments do |t|
      t.references :project, foreign_key: true
      t.references :attachment, foreign_key: true

      t.timestamps
    end
  end
end
