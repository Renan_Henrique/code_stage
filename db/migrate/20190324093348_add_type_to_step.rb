class AddTypeToStep < ActiveRecord::Migration[5.2]
  def change
    add_reference :steps, :type, foreign_key: true
  end
end
