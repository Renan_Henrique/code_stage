class AddTypeToOccupation < ActiveRecord::Migration[5.2]
  def change
    add_column :occupations, :type, :string
  end
end
