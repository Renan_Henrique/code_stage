# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

user1 = User.create(email: "julia@email.com", password: "123123", name:"Julia", password_confirmation: "123123")
user2 = User.create(email: "lucia@email.com", password: "123123", name:"Lucia", password_confirmation: "123123")
user3 = User.create(email: "rita@email.com", password: "123123", name:"Rita", password_confirmation: "123123")
user4 = User.create(email: "anna@email.com", password: "123123", name:"Anna", password_confirmation: "123123")
user5 = User.create(email: "luiza@email.com", password: "123123", name:"Luiza", password_confirmation: "123123")
user6 = User.create(email: "sonia@email.com", password: "123123", name:"Sônia", password_confirmation: "123123")

occupation4 = Occupation.create(title: "Advogada", moment: "makers")
occupation3 = Occupation.create(title: "Designer", moment:"makers")
occupation1 = Occupation.create(title: "Marketing", moment:"makers")
occupation5 = Occupation.create(title: "Administradora", moment:"makers")

occupation2 = Occupation.create(title: "Intérprete", moment:"creators")
Occupation.create(title: "Compositora", moment:"creators")
Occupation.create(title: "Produtora", moment:"creators")
Occupation.create(title: "Musicista", moment:"creators")
Occupation.create(title: "Engenheira de som", moment:"creators")

user1.occupations << occupation1
user2.occupations << occupation2
user3.occupations << occupation3
user4.occupations << occupation4
user5.occupations << occupation5
user6.occupations << occupation4

Type.create(title: "Pocket show")
Type.create(title: "Produção single")
Type.create(title: "Divulgação")

Project.create(title: "Produção de demo de punk rock", description: "Demo da mais nova banda de punk Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make", user_id: 2)
Project.create(title: "Show em homenagem ao movimento hippie", description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make", user_id: 2)