import React from 'react'
import {Switch, Route} from 'react-router-dom'
import {
  Profile,
  Test,
  Timeline
} from '../views'

const Routes = () => (
  <Switch>
    <Route exact path="/profile" component={Profile} />
    <Route exact path="/teste" component={Test} />
    <Route exact path="/" component={Timeline} />
  </Switch>
)

export default Routes
