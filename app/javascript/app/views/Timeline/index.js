import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './Timeline.scss'
import { LeftMenu, Feed, RightMenu, HeaderMenu } from 'components'
export class Timeline extends Component {
  static propTypes = {

  }

  render() {
    return (
      <div className="container-fluid Timeline">
        <HeaderMenu />
        <div className="Timeline-container">
          <LeftMenu />
          <Feed />
          <RightMenu />
        </div>        
      </div>
    )
  }
}

export default Timeline
