import React, { Component } from 'react'
import PropTypes from 'prop-types'
import InputFiles from 'react-input-files'
import axios from 'axios'

export class Profile extends Component {
  static propTypes = {

  }

  handleSubmit(file) {
    axios.post('http://localhost:3000/attachments')
    const config = {headers: {'Content-Type': 'multipart/form-data'}}
    let formData = new FormData()
    formData.append('file', file)
    axios.post('/attachments', formData, config)
    event.preventDefault();
  }

  render() {
    return (
      <div>
        {/* <form onSubmit={this.handleSubmit}> */}
          <InputFiles multiple={false} onChange={files => this.handleSubmit(files[0])} >
            <button>Uploda</button>
          </InputFiles>
        {/* </form> */}
      </div>
    )
  }
}

export default Profile
