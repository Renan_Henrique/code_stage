import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './HeaderMenu.scss'
import logo from 'images/areta.png'
export class HeaderMenu extends Component {
  static propTypes = {

  }

  render() {
    return (
      <div className="HeaderMenu">
        <div className="HeaderMenu-logo" style={{backgroundImage: 'url(' + logo + ')'}}>

        </div>
      </div>
    )
  }
}

export default HeaderMenu
