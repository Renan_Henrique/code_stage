import React, { Component } from 'react'
import PropTypes from 'prop-types'
import './LeftMenu.scss'
import vectorPerfil from 'images/Vector_perfil.png'
import pessoa1 from 'images/Pessoa1.jpg'
import { PublishProject } from 'components'
export class LeftMenu extends Component {

  constructor(props) {
    super(props)
    this.state = {
      items: [
        {'id': 0, 'name': 'Composição'},
        {'id': 1, 'name': 'Musica 1'},        
      ],
      modalStatus: false
    }
  }

  renderItem = (item) => {
    return <div className="LeftMenu-container-items-item">
      {item.name}
    </div>
  }

  renderModal = () => {
    this.setState({
      modalStatus: !this.state.modalStatus
    })
  }


  render() {
    return (
      <div className="LeftMenu" style={{backgroundImage: 'url(' + vectorPerfil + ')'}}>
        <div className="LeftMenu-image" style={{backgroundImage: 'url(' + vectorPerfil + ')'}} ></div>
        <div className="LeftMenu-top">
          <div className="LeftMenu-top-profile">
            <div className="LeftMenu-top-profile-item">
              <div className="LeftMenu-top-profile-item-avatar" style={{backgroundImage: 'url(' + pessoa1 + ')'}}>

              </div>

              <div className="LeftMenu-top-profile-item-items">

                <div className="LeftMenu-top-profile-item-items-name">
                  name
                </div>

                <div className="LeftMenu-top-profile-item-items-cargo">
                  Cargos
                </div>
              </div>
            </div>

            <div className="LeftMenu-top-profile-data"> 
              <div className="LeftMenu-top-profile-data-email">
                email@email.com.br
              </div>

              <div className="LeftMenu-top-profile-data-city">
                Cargo da pessoa
              </div>

            </div>


          </div>
          <div className="LeftMenu-top-description">
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.
          </div>
          <div className="LeftMenu-top-quads">
            <div className="LeftMenu-top-quads-quad"></div>
            <div className="LeftMenu-top-quads-quad"></div>
            <div className="LeftMenu-top-quads-quad"></div>
          </div>
        </div>
        <div className="LeftMenu-container">
          <span className="LeftMenu-container-title">Projetos</span>
          <div className="LeftMenu-container-items">
            {
              this.state.items.map((item)=>{
                return this.renderItem(item);
              })
            }

            <div className="LeftMenu-container-items-add" onClick={()=> this.renderModal()}>
              +
            </div>

            <PublishProject modalChange={this.renderModal} statusModal={this.state.modalStatus}/>
          </div>
        </div>
        <div className="LeftMenu-bottom">
          Termos e condições | FAQ
        </div>
      </div>
    )
  }
}

export default LeftMenu
