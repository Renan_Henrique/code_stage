import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Creators as animAction} from 'redux/reducers/animations'
import {Creators as feedAction} from 'redux/reducers/feed'
import './Feed.scss'
import { PublishProject } from 'components'
import { runInThisContext } from 'vm';
import feedAvatar from 'images/Pessoa2.jpg'
import moreIcon from 'images/more.svg'

export class Feed extends Component {
  static propTypes = {

  }

  componentDidMount = () => {
    this.props.getMyFeed()
  }
  returnPost = (feed) => {
    return <div className="Feed-container-post">

        <div className="Feed-container-post-item">
            <div className="Feed-container-post-item-avatar" style={{backgroundImage: 'url(' + feedAvatar + ')'}}>

            </div>

            <div className="Feed-container-post-item-name">
                {feed.users_informations.name}
            </div>
        </div>

        <div className="Feed-container-post-content">
            <div className="Feed-container-post-content-title">{feed.title}</div>
            <div className="Feed-container-post-content-description"> {feed.description}</div>                
        </div>

        <div className="Feed-container-post-line">
            <div className="Feed-container-post-like">
                XX Like
            </div>
        </div>
    </div>
  }

  render() {
    var newClass = this.props.animationStatus ? "open" : "close";
    return (
      <div target={newClass} className="Feed">
        <div className="Feed-show" style={{backgroundImage: 'url(' + moreIcon + ')'}}  onClick={()=>{this.props.setStatus(!this.props.animationStatus)}}>
        
        </div>
        <div className="Feed-top">
            <input placeholder= "Search" className="Feed-top-search"/>
            <span className="Feed-top-title">Publicações recentes</span>
        </div>
        <div className="Feed-container"> 
            
            {
                this.props.feeds.map((feed) => {
                    return this.returnPost(feed)
                })
            }

        </div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    animationStatus: state.animations.status,
    feeds: state.feed.feeds,
  }
}
  
export default connect(mapStateToProps, {...animAction, ...feedAction})(Feed)