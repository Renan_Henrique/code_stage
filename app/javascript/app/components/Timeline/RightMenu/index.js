import React, { Component } from 'react'
import PropTypes from 'prop-types'
import {connect} from 'react-redux'
import {Creators as animAction} from 'redux/reducers/animations'
import './RightMenu.scss'
import moreIcon from 'images/more.svg'
import pessoa1 from 'images/Pessoa1.jpg'
import pessoa2 from 'images/Pessoa2.jpg'
import pessoa3 from 'images/Pessoa3.jpg'
import pessoa4 from 'images/Pessoa4.jpg'
import pessoa5 from 'images/Pessoa5.jpg'
export class RightMenu extends Component {
  static propTypes = {

  }

  render() {
    var newClass = this.props.animationStatus ? "open" : "close";
    return (        
      <div className="RightMenu" target={newClass}  >
        <div className="RightMenu-wallet">
            <div className="RightMenu-wallet-title">
                Minha conta
            </div>

            <div className="RightMenu-wallet-money"> 
                <span className="RightMenu-wallet-money-real">
                    R$
                </span>
                <span className="RightMenu-wallet-money-value">
                    15.000,00
                </span>
            </div>

            <span className="RightMenu-wallet-subtitle">
                Saldo em 24 de março de 2019
            </span>

            <span className="RightMenu-wallet-title">
                Meu investimento
            </span>

            <div className="RightMenu-wallet-money">
                <span className="RightMenu-wallet-money-real">
                    R$
                </span>
                <span className="RightMenu-wallet-money-value">
                    7.000,00
                </span>
            </div>

            <span className="RightMenu-wallet-pay">
                Pagamentos em andamento
            </span>

            <div className="RightMenu-wallet-peoples">
                <div className="RightMenu-wallet-peoples-people">
                    <div className="RightMenu-wallet-peoples-people-photo" style={{backgroundImage: 'url(' + pessoa2 + ')'}}> </div>
                    <div className="RightMenu-wallet-peoples-people-name">Lucia </div> 
                </div>
                <div className="RightMenu-wallet-peoples-people">
                    <div className="RightMenu-wallet-peoples-people-photo" style={{backgroundImage: 'url(' + pessoa3 + ')'}}> </div>
                    <div className="RightMenu-wallet-peoples-people-name">Fernanda Soarez</div> 
                </div>
                <div className="RightMenu-wallet-peoples-people">
                    <div className="RightMenu-wallet-peoples-people-photo" style={{backgroundImage: 'url(' + pessoa4 + ')'}}> </div>
                    <div className="RightMenu-wallet-peoples-people-name">Bianca Guin </div> 
                </div>
            </div>

            <span className="RightMenu-wallet-pay">
                Pagamentos finalizados
            </span>

            <div className="RightMenu-wallet-peoples">
                <div className="RightMenu-wallet-peoples-people">
                    <div className="RightMenu-wallet-peoples-people-photo" style={{backgroundImage: 'url(' + pessoa5 + ')'}}> </div>
                    <div className="RightMenu-wallet-peoples-people-name">Brenda Gomes Teixeira </div> 
                </div>
                <div className="RightMenu-wallet-peoples-people">
                    <div className="RightMenu-wallet-peoples-people-photo" style={{backgroundImage: 'url(' + pessoa2 + ')'}}> </div>
                    <div className="RightMenu-wallet-peoples-people-name">Lucia </div> 
                </div>
                <div className="RightMenu-wallet-peoples-people">
                    <div className="RightMenu-wallet-peoples-people-photo" style={{backgroundImage: 'url(' + pessoa2 + ')'}}> </div>
                    <div className="RightMenu-wallet-peoples-people-name">Lucia </div> 
                </div>
            </div>
            {/* <div className="RightMenu-wallet-back" style={{backgroundImage: 'url(' + moreIcon + ')'}}>
                
            </div> */}
        </div>

      </div>
    )
  }
}



const mapStateToProps = state => ({
    animationStatus: state.animations.status,
})
  
export default connect(mapStateToProps, animAction)(RightMenu)
