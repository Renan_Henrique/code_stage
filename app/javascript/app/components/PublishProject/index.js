import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import {Creators as feedAction} from 'redux/reducers/feed'
import ReactModal from 'react-modal'
import './PublishProject.scss'
import {

} from 'components'
class PublishProject extends Component {
  constructor() {
    super()
    this.state={
      checkboxValue: false,
    }
  }

  render() {
    var current = "";
    return [
      <ReactModal
        
        shouldCloseOnEsc={true}
        shouldCloseOnOverlayClick={true}
        ariaHideApp={false}
        isOpen={this.props.statusModal}        
        className={"PublishProject"}
      >
        <div className="PublishProject-container">
          <div className="PublishProject-container-left">
            <span className="PublishProject-container-left-title"> Projeto </span>
            <input className="PublishProject-container-left-input" placeholder="Musica 1"/>  

            <div className="PublishProject-container-left-description">

            </div>
            <div className="PublishProject-container-left-profile">
              <div className="PublishProject-container-left-profile-avatar">

              </div>

              <div className="PublishProject-container-left-profile-name">
              
              </div>

              <div className="PublishProject-container-left-profile-cargo">

              </div>
            </div>       
          </div>

          <div className="PublishProject-container-right">
            <div className="PublishProject-container-right-top">
              <input className="PublishProject-container-right-top-input" placeholder="Descrição"/>
            </div>

            <div className="PublishProject-container-right-center">
              <div className="PublishProject-container-right-center-description">Conteudo DESCRICAO</div>
              <div className="PublishProject-container-right-center-subtitle">Profissões checkbox</div>
              <div className="PublishProject-container-right-center-Checkboxs">
                <input
                  className="myCheckbox"
                  type="checkbox"
                  onChange={() => console.log(data)}
                  checked={this.state.checkboxValue}
                />
                <input
                  className="myCheckbox"
                  type="checkbox"
                  onChange={() => console.log(data)}
                  checked={this.state.checkboxValue}
                />
                <input
                  className="myCheckbox"
                  type="checkbox"
                  onChange={() => console.log(data)}
                  checked={this.state.checkboxValue}
                />
                <input
                  className="myCheckbox"
                  type="checkbox"
                  onChange={() => console.log(data)}
                  checked={this.state.checkboxValue}
                />
                <input
                  className="myCheckbox"
                  type="checkbox"
                  onChange={() => console.log(data)}
                  checked={this.state.checkboxValue}
                />
                <input
                  className="myCheckbox"
                  type="checkbox"
                  onChange={() => console.log(data)}
                  checked={this.state.checkboxValue}
                />
                <input
                  className="myCheckbox"
                  type="checkbox"
                  onChange={() => console.log(data)}
                  checked={this.state.checkboxValue}
                />
                <input
                  className="myCheckbox"
                  type="checkbox"
                  onChange={() => console.log(data)}
                  checked={this.state.checkboxValue}
                />
              </div>
            </div>
            <div className="PublishProject-container-right-bottom">
              <div className="PublishProject-container-right-bottom-tags">
                <span className="PublishProject-container-right-bottom-tags-title">
                  Tags
                </span>
                <div className="PublishProject-container-right-bottom-tags-list">
                  <div className="PublishProject-container-right-bottom-tags-list-item">
                    Tag
                  </div>
                </div>
              </div>
              <div className="PublishProject-container-right-bottom-buttons">
                <div className="PublishProject-container-right-bottom-buttons-left" onClick={()=>{
                  this.props.postProject();
                }}>
                  Publicar
                </div>

                <div className="PublishProject-container-right-bottom-buttons-right">
                  Próximo
                </div>
              </div>
            </div>

          </div>
        </div>
      </ReactModal>
    ]
  }
}
const mapStateToProps = state => {
  return {
    inPublishing: state.feed.inPublishing,
  }
}
export default connect(mapStateToProps, {...feedAction})(PublishProject)
