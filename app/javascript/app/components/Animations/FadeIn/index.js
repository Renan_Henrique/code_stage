import React from 'react'
import './FadeIn'

const FadeIn = props => (
  <div className="FadeIn" style={{animationDelay: 0.05 * props.index + 's'}}>
    {props.children}
  </div>
)

export default FadeIn
