import React, {Component} from 'react'
import './DiagonalControl'

class DiagonalControl extends Component {
  state = {
    animationStatus: 'auto',
  }

  updateStatus = flag => {
    this.setState({animationStatus: flag ? 'auto' : 0})
  }

  render() {
    const {status, children} = this.props
    return (
      <div
        id="DiagonalControl"
        className={status ? 'DiagonalAppear' : 'DiagonalDisappear'}
        style={{
          display: status == undefined && 'none',
          height: this.state.animationStatus,
        }}
        onAnimationEnd={() => this.updateStatus(status)}
      >
        {children}
      </div>
    )
  }
}

export default DiagonalControl
