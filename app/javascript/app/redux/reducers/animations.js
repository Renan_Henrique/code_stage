export const Types = {
    SET_STATUS: 'animations/SET_STATUS',
  }
  
  export const Creators = {
    setStatus: (status) => {    
        return {
          type: Types.SET_STATUS,
          payload: status,
        }
      }
  }
  
  const INITIAL_STATE = {
      status: true
  }
  
  export default function animations(state = INITIAL_STATE, action) {
    switch (action.type) {
        case Types.SET_STATUS:
        return {
          ...state,
          status: action.payload,
        }

      default:
        return state
    }
  }