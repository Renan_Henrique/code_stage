import API from 'configs/API'

export const Types = {
  GET_FEED: 'feed/GET_FEED',
  POST_PROJECT: 'feed/POST_PROJECT'
}

export const Creators = {
  getMyFeed: () => {
    const response = API.get('/feed/that_i_can_see')
    return {
      type: Types.GET_FEED,
      payload: response,
    }
  },
  postProject: () => {
    const response = API.post('/projects', {
      id: 2,
      title: 'Segunda música',
      description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.',
      makers_id: [1, 3]
    })
    return {
      type: Types.POST_PROJECT,
      payload: response,
    }
  },
}


const INITIAL_STATE = {
  feeds:[],
  inPublishing: ''
}

export default function feed(state = INITIAL_STATE, action) {
  switch (action.type) {
    case Types.GET_FEED:
      return {
        ...state,
        feeds: action.payload.data
      }

    case Types.POST_PROJECT:
      return {
        ...state,
        inPublishing: action.payload.data
      }
    default:
      return state
  }
}
