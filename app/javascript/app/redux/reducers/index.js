import {combineReducers} from 'redux'
import initial from './initial'
import feed from './feed'
import animations from './animations'

export default combineReducers({
  initial,
  feed,
  animations
})
