export const Types = {
  GET_MEDIAS_ON_CHAT_ROOM: 'mediasChat/GET_MEDIAS_ON_CHAT_ROOM',
}

export const Creators = {
}

const INITIAL_STATE = {
}

export default function initial(state = INITIAL_STATE, action) {
  switch (action.type) {
    default:
      return state
  }
}
