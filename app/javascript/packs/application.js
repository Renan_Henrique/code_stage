import React from 'react'
import ReactDOM from 'react-dom'
import PropTypes from 'prop-types'
import Routes from '../app/routes/routes'
import store from '../app/redux/store'
import {Provider} from 'react-redux'
import {BrowserRouter} from 'react-router-dom'
import '../app/scss/index.scss'
document.addEventListener('DOMContentLoaded', () => {
  ReactDOM.render(
    <BrowserRouter>
        <Provider store={store}>  
          <Routes />
        </Provider>    
      </BrowserRouter>
    ,
    document.getElementById('app')
  )
})