class Project < ApplicationRecord
  has_one :user
  has_one :project_attachment
  has_one :attachment, through: :project_attachment

  has_many :step_projects
  has_many :steps, through: :step_projects

  has_many :tag_projects
  has_many :tags, through: :tag_projects

  has_many :project_types
  has_many :types, through: :project_types

  def step_projects
    self.steps
  end

  def tag_projects
    Occupation.where(moment: "creators")
  end

  def project_types
    self.types
  end

  def users_informations
    User.find self.user_id
  end
end
