class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  has_many :submissions
  
  has_many :projects

  has_many :user_occupations
  has_many :occupations, through: :user_occupations

  has_many :user_skills
  has_many :skills, through: :user_skills

  has_many :user_goals
  has_many :goals, through: :user_goals

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  
  def projects
    submissions = self.submissions
    submissions
  end


  def self.map_reponse

    byebug 
    # if self.occupations[0]['moment'] != "creators" 
    #   return
    # end
    # resp = {
    #   "user" => self,
    #   "occupation" => self.occupation
    # }
  end
end
