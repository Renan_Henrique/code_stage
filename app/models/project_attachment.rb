class ProjectAttachment < ApplicationRecord
  belongs_to :project
  belongs_to :attachment
end
