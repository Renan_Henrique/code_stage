class FeedsController < ApplicationController
  def informations
    render json: current_user, methods: [:projects ] 
  end

  def submission_that_i_can_see
    @stics = Project.all.where.not(user_id: current_user.id)
    render json: @stics, methods: [:users_informations]
  end
end
