class AttachmentsController < ApplicationController
  def new

  end

  def create
    byebug
    @attachment = Attachment.new()
    @attachment.image.attach(params[:files])
    
    if @attachment.save
      render json: @attachment, status: :created, location: @attachment
    else 
      render json: @attachment.errors
    end
  end
end
