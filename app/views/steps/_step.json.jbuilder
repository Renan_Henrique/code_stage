json.extract! step, :id, :title, :description, :created_at, :updated_at
json.url step_url(step, format: :json)
