Rails.application.routes.draw do
  # get 'skills/home'
  # get 'steps/home'
  resources :projects
  root to: "pages#home"
  devise_for :users

  resources :attachments

  get '/feed/user/informations', to:"feeds#informations"
  get '/feed/that_i_can_see', to: "feeds#submission_that_i_can_see"
  get '/project/need', to: "projects#project_needs"
  get '*path', to: "pages#home"
end
